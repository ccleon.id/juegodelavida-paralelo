#include <stdio.h>
#include <string.h>
#include <time.h>
#include "board.h"
#include "def.h"

void board_mostrar(board_t *board){
  HEADER
  for(size_t i = 0; i < board->row*board->col; i++){
    if(i%board->row==0 && i>0)printf(" ║\n║");
    if(board->tablero[i])printf(" ■");
    else{printf(" ▢");}
  }
  FOTTER
}

// printf("x=%d,y=%d\n",(((i/5)-1)+5)%5,(((i%5)-1)+5)%5);

int cantvecinas(board_t *board,size_t i){
  size_t c = board->col;
  size_t f = board->row;
  char* t = board->tablero;
  int  ret=0;
  //           _cfil_              _ccol_
  ret += t[f*((((i/f)-1)+f)%f)+((((i%c)-1)+c)%c)];
  ret += t[f*((((i/f)-1)+f)%f)+((((i%c)-0)+c)%c)];
  ret += t[f*((((i/f)-1)+f)%f)+((((i%c)+1)+c)%c)];

  ret += t[f*((((i/f)+0)+f)%f)+((((i%c)-1)+c)%c)];
  ret += t[f*((((i/f)+0)+f)%f)+((((i%c)+1)+c)%c)];

  ret += t[f*((((i/f)+1)+f)%f)+((((i%c)-1)+c)%c)];
  ret += t[f*((((i/f)+1)+f)%f)+((((i%c)+0)+c)%c)];
  ret += t[f*((((i/f)+1)+f)%f)+((((i%c)+1)+c)%c)];
  return ret;
}

void update(board_t *board){
  char temp[board->row*board->col];
  for(size_t i = 0; i < board->row*board->col; i++){
    temp[i]=board->tablero[i];
    int n=cantvecinas(board,i);
    // printf("i=%d,%d,",i,n );
    // while ( getchar() != '\n');
    if(n<2 || n>3)temp[i]='\0';
    else if(n==3)temp[i]='\1';
  }
  board_load(board,temp);
}

int main(int argc, char const *argv[]) {
  board_t *board = malloc(sizeof(struct _board));
  board_init(board,20,20);
  board_load(board,INITCHAR);
  board_set(*board,1,0,'\1');
  board_set(*board,1,1,'\1');
  board_set(*board,1,2,'\1');

  board_mostrar(board);
  update(board);
  board_mostrar(board);

  while (1) {
    sleep(0.01);
    system("clear");
    update(board);
    board_mostrar(board);
  }


  return 0;
}
