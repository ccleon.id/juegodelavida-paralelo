#include "board.h"
#include <string.h>

int board_init(board_t *board, size_t col, size_t row){
  board->tablero = malloc(sizeof(char) * (row*col));
  board->col = col;
  board->row = row;

  for(size_t i = 0; i < board->row*board->col; i++){
    board->tablero[i]='\0';
  }

  return 1;
}

int board_load(board_t *board, char *str){
  memcpy(board->tablero,str,sizeof(char)*(board->col*board->row));
  return 1;
}

char board_get(board_t board, size_t col, size_t row){
  return board.tablero[(board.row*row)+col];
}

int board_set(board_t board, size_t col, size_t row, char val){
   board.tablero[(board.row*row)+col]=val;
   return 1;
}
